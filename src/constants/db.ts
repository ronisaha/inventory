export const DB_CONFIG = {
        name: 'ez_inventory',
        tables: [
            {
                name: 'system_users',
                columns: [
                    {name: 'username', type: 'TEXT PRIMARY KEY NOT NULL UNIQUE'},
                    {name: 'fullName', type: 'TEXT'},
                    {name: 'password', type: 'TEXT'},
                    {name: 'gender', type: 'TEXT'},
                    {name: 'photo', type: 'TEXT'}
                ]
            },
            {
                name: 'products',
                columns: [
                    {name: 'id', type: 'INTEGER PRIMARY KEY AUTOINCREMENT'},
                    {name: 'name', type: 'TEXT'},
                    {name: 'description', type: 'TEXT'},
                    {name: 'price', type: 'INTEGER'},
                    {name: 'stock', type: 'INTEGER'},
                    {name: 'photo', type: 'TEXT'}
                ]
            },
            {
                name: 'sold_items',
                columns: [
                    {name: 'id', type: 'INTEGER PRIMARY KEY AUTOINCREMENT'},
                    {name: 'product_id', type: 'INTEGER'},
                    {name: 'price', type: 'TEXT'},
                    {name: 'count', type: 'INTEGER'}
                ]
            }
        ]
    };