import {AccountPage} from "../pages/account/account";
import {ProductsPage} from "../pages/products/products";
import {SalesPage} from "../pages/sales/sales";

export const APP_MENUS = {
    sidebar: [
        {
            label: 'Account',
            cache: false,
            icon: 'md-person',
            component: AccountPage
        },
        {
            label: 'Products',
            cache: false,
            icon: 'md-cube',
            component: ProductsPage
        },
        {
            label: 'Sold Items',
            cache: false,
            icon: 'ios-cube-outline',
            component: SalesPage
        }
    ]
};
