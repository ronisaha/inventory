
export class Product {

  private _id:number = null;
  private _name:string;
  private _description:string;
  private _price:number;
  private _stock:number;
  private _photo = "";

  constructor(parameters: {id?: number, name?: string, description?: string, price?: number, stock?: number, photo?: string}) {
    let {id, name, description, price, stock, photo} = parameters;
    this._id = id || null;
    this._name = name;
    this._description = description;
    this._price = price;
    this._stock = stock;
    this._photo = photo;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }


  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get price(): number {
    return this._price;
  }

  set price(value: number) {
    this._price = value;
  }

  get stock(): number {
    return this._stock;
  }

  set stock(value: number) {
    this._stock = value;
  }

  get photo(): string {
    return this._photo;
  }

  set photo(value: string) {
    this._photo = value;
  }
}
