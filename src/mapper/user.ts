
export class User {

  private _username:string;
  private _fullName:string;
  private _password:string;
  private _gender:string;
  private _photo = "";


  constructor(parameters: {username?: string, fullName?: string, password?: string, gender?: string, photo?: string}) {
    let {username, fullName, password, gender, photo} = parameters;
    this._username = username;
    this._fullName = fullName;
    this._password = password;
    this._gender = gender;
    this._photo = photo;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get fullName(): string {
    return this._fullName;
  }

  set fullName(value: string) {
    this._fullName = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get gender(): string {
    return this._gender;
  }

  set gender(value: string) {
    this._gender = value;
  }

  get photo(): string {
    return this._photo && this._photo != "undefined" && this._photo != "" ? this._photo : null;
  }

  set photo(value: string) {
    this._photo = value;
  }
}
