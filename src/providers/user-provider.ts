import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {DbProvider} from "./db-provider";
import {User} from "../mapper/user";

const USER_CREATE_QUERY = "INSERT OR REPLACE INTO system_users " +
    "(username, fullName, password, gender, photo) VALUES (?, ?, ?, ?, ?)";

const SELECT_BY_USERNAME = "SELECT * FROM system_users WHERE username like (?)";

@Injectable()
export class UserProvider {

    constructor(private db: DbProvider) {}

    get(username): Promise<User> {
        return this.getByUserName(username);
    }

    getByUserName(username:string): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            this.db.query(SELECT_BY_USERNAME, [username])
                .then(result => resolve(this.db.getSingleResult(result)), reject);
        });
    }

    create(u:User): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            this.db.query(USER_CREATE_QUERY, [u.username, u.fullName, u.password, u.gender, u.photo])
                .then(result => {console.log(result); resolve(u)}, reject);
        });
    }
}
