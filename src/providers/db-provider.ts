import {Injectable} from '@angular/core';
import {SQLite} from 'ionic-native';
import 'rxjs/add/operator/map';
import {Platform} from "ionic-angular";
import {DB_CONFIG} from "../constants/db";

@Injectable()
export class DbProvider {
    private _db: any;
    private _w;

    constructor(public platform: Platform) {
        this._w = window;
        this.platform.ready().then(() => this.init());
    }

    getSingleResult(result) {
        if (result.rows.length != 1) {
            return null;
        }
        return result.rows.item(0);
    };

    getList(result): Array<any> {
        let output = [], i;

        for (i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }

        return output;
    }

    init() {
        this.openDB().then(db => {
            this._db = db;
            this.createDatabases();
        });
    }

    private createDatabases() {
        let query = [];
        DB_CONFIG.tables.forEach(function (table) {
            let columns = [];

            table.columns.forEach(function (column) {
                columns.push(column.name + ' ' + column.type);
            });

            query.push('CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')');

        });

        this.runAllQuery(query);
    }

    private runAllQuery(queries: string[], i: number = 0) {
        if (queries[i]) {
            this.query(queries[i], []).then(() => {
                this.runAllQuery(queries, i + 1);
            });
        }
    }

    openDB(): Promise<any> {
        if (this._db != null) {
            return Promise.resolve(this._db);
        }

        return new Promise((resolve, reject) => {
            SQLite.echoTest().then(
                success => {
                    SQLite.openDatabase({name: DB_CONFIG.name + ".db", location: 'default'})
                        .then(db => resolve(db));
                },
                err => {
                    try {
                        let db = this._w.openDatabase(DB_CONFIG.name + ".db", '1.0', 'database', -1);
                        resolve(db);
                    } catch (e) {
                        reject(e);
                    }
                }
            );
        });
    }

    public transaction(fn: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.openDB().then(db => {
                db.transaction(fn, resolve, reject);
            })
        });
    }

    public query(sql: string, parameters: any[] = []): Promise<any> {
        return new Promise((resolve, reject) => {
            this.openDB().then(db => {
                db.transaction(function (tx) {
                    tx.executeSql(sql, parameters, function (tx, results) {
                        resolve(results);
                    }, reject);
                });
            })
        });
    }
}
