import {Injectable, EventEmitter} from '@angular/core';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import {Platform} from "ionic-angular";
import {User} from "../mapper/user";
import {LOCAL_TOKEN_KEY} from "../constants/auth";
import {UserProvider} from "./user-provider";

@Injectable()
export class AuthProvider {
  private _onSessionStore = new EventEmitter<any>();
  private _currentUser:User;

  private _isAuthenticated = false;
  private _loaded = false;

  constructor(public platform: Platform, private storage: Storage, private userProvider:UserProvider) {
    this.platform.ready().then(() => {
        this.loadUserCredentials();
    });
  }

  ready():Promise<any> {

    if(this._loaded) {
      return Promise.resolve();
    }

    return new Promise((resolve)=>{
      let a = this.onSessionStore.subscribe(()=>{
          resolve();
          a.unsubscribe();
      });
    });
  }

  get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }

  get onSessionStore(): EventEmitter<any> {
    return this._onSessionStore;
  }

  loadUserCredentials() {
    this.storage.get(LOCAL_TOKEN_KEY)
        .then(
            username => {this.useCredentials(username)},
            error => {console.error(error);  this.destroyUserCredentials();}
        );
  }

  useCredentials(username) {
    this.getClaimsFromToken(username).then(u => {
      this.registerUserSession(u);
    });
  }

  registerUserSession(u) {
    this._currentUser = u;
    this._isAuthenticated = this._currentUser != null;
    this._loaded = true;
    this.onSessionStore.emit();
  }

  private getClaimsFromToken(username): Promise<User> {
    if (typeof username !== 'undefined' && username != null) {
      return this.userProvider.get(username);
    } else {
      return Promise.resolve(null);
    }
  }

  private storeUserCredentials(user:User) {
    if (user == null || user.username == null) {
      this.destroyUserCredentials();
    } else {
      this.storage.set(LOCAL_TOKEN_KEY, user.username)
          .then(
              data => console.log(data),
              error => console.error(error)
          );

      this.registerUserSession(user);
    }
  }

  destroyUserCredentials() {
    this._currentUser = new User({});
    this._isAuthenticated = false;
    this.storage.remove(LOCAL_TOKEN_KEY)
        .then(
            data => {this.onSessionStore.emit();},
            error => {this.onSessionStore.emit();}
        );
  }


  get currentUser(): User {
    return this._currentUser;
  }

  login(username: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.userProvider.getByUserName(username).then(
          u => {
            if (u != null && u.password == password) {
              this.storeUserCredentials(u);
              resolve('logged in')
            } else {
              reject('failed')
            }
          },
          err => reject(err)
      )
    });
  }
}
