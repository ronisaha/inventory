import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {DbProvider} from "./db-provider";
import {Product} from "../mapper/product";

const CREATE_QUERY = "INSERT OR REPLACE INTO products " +
    "(id, name, description, price, stock, photo) VALUES (?, ?, ?, ?, ?, ?)";

const SELECT_BY_USERNAME = "SELECT * FROM products WHERE id = (?)";
const ADD_STOCK_QUERY = "UPDATE products set stock = stock + (?) WHERE id = (?)";
const SELL_STOCK_QUERY = "UPDATE products set stock = stock - (?) WHERE id = (?)";
const ADD_SELL_QUERY = "INSERT INTO sold_items (product_id, price, count)  VALUES (?, ?, ?)";
const ALL_SOLD_ITEMS = "select sold_items.id, products.name, products.description, sold_items.count, sold_items.price * sold_items.count as price " +
    "from sold_items join products on(products.id = sold_items.product_id)";

@Injectable()
export class ProductProvider {

  constructor(private db: DbProvider) {}

  get(username): Promise<Product> {
    return this.getByUserName(username);
  }

  all(): Promise<Array<Product>> {
    return new Promise<Array<Product>>((resolve, reject) => {
      this.db.query("SELECT * FROM products")
          .then(result => resolve(this.db.getList(result)), reject);
    });
  }

  allSells(): Promise<Array<Product>> {
    return new Promise<Array<Product>>((resolve, reject) => {
      this.db.query(ALL_SOLD_ITEMS)
          .then(result => resolve(this.db.getList(result)), reject);
    });
  }

  addStock(id, quantity):Promise<Product> {
    return new Promise<Product>((resolve, reject) => {
      this.db.query(ADD_STOCK_QUERY, [quantity, id])
        .then(result => console.log(result), reject);
    });
  }

  sellProduct(id, price, quantity):Promise<Product> {
    return new Promise<Product>(() => {
        this.db.transaction(function (tx) {
          tx.executeSql(SELL_STOCK_QUERY, [quantity, id]);
          tx.executeSql(ADD_SELL_QUERY, [id, price, quantity]);
        });
    });
  }

  getByUserName(username:string): Promise<Product> {
    return new Promise<Product>((resolve, reject) => {
      this.db.query(SELECT_BY_USERNAME, [username])
          .then(result => resolve(this.db.getSingleResult(result)), reject);
    });
  }

  create(p:Product): Promise<Product> {
    return new Promise<Product>((resolve, reject) => {
      this.db.query(CREATE_QUERY, [p.id, p.name, p.description, p.price, p.stock, p.photo])
          .then(result => {console.log(result.insertId);p.id = result.insertId; resolve(p)}, reject);
    });
  }
}
