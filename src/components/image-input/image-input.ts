import {Component, Input, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {Camera, CameraOptions} from 'ionic-native';

class ImageInputOption {
    /** Picture quality in range 0-100. Default is 50 */
    quality?: number;
    /**
     * Set the source of the picture.
     * Defined in Camera.PictureSourceType. Default is CAMERA.
     *      PHOTOLIBRARY : 0,
     *      CAMERA : 1,
     *      SAVEDPHOTOALBUM : 2
     */
    sourceType?: number;
    /** Allow simple editing of image before selection. */
    allowEdit?: boolean;
    /**
     * Choose the returned image file's encoding.
     * Defined in Camera.EncodingType. Default is JPEG
     *      JPEG : 0    Return JPEG encoded image
     *      PNG : 1     Return PNG encoded image
     */
    encodingType?: number;
    /**
     * Width in pixels to scale image. Must be used with targetHeight.
     * Aspect ratio remains constant.
     */
    targetWidth?: number;
    /**
     * Height in pixels to scale image. Must be used with targetWidth.
     * Aspect ratio remains constant.
     */
    targetHeight?: number;
}

@Component({
    selector: 'image-input',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ImageInputComponent),
            multi: true
        }
    ],
    template: `
<div *ngIf="imageValue" text-center style="height: 200px;"><img [src]="imageValue"  height="100%" /></div>
<button ion-button (click)="captureImage()" block><ng-content></ng-content></button>
`
})
export class ImageInputComponent implements ControlValueAccessor {

    @Input("value") private _imageValue = "";

    @Input("option") set option(val:ImageInputOption) {
        this.captureOption.quality = val.quality || this.captureOption.quality;
        this.captureOption.sourceType = val.sourceType || this.captureOption.sourceType;
        this.captureOption.allowEdit = val.allowEdit || this.captureOption.allowEdit;
        this.captureOption.encodingType = val.encodingType || this.captureOption.encodingType;
        this.captureOption.targetWidth = val.targetWidth || this.captureOption.targetWidth;
        this.captureOption.targetHeight = val.targetHeight || this.captureOption.targetHeight;
    };

    private captureOption: CameraOptions = {
        quality: 75,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 400,
        targetHeight: 400,
        correctOrientation: true,
        saveToPhotoAlbum: false,
    };

    propagateChange = (_: any) => {
    };

    writeValue(value: any) {
        if (value !== undefined) {
            this.imageValue = value;
        } else {
            this.imageValue = ""
        }
    }

    get imageValue(): string {
        return this._imageValue;
    }

    set imageValue(value: string) {
        this._imageValue = value;
        this.propagateChange(this._imageValue);
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    captureImage() {
        Camera.getPicture(this.captureOption).then((imageData) => {
            this.imageValue = 'data:image/jpeg;base64,' + imageData;
            Camera.cleanup().then();
        });

        return false;
    }

    registerOnTouched() {
    }
}
