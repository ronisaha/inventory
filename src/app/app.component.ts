import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar, Splashscreen} from 'ionic-native';
import {LoginPage} from "../pages/login/login";
import {AuthProvider} from "../providers/auth-provider";
import {APP_MENUS} from "../constants/menu";
import {AccountPage} from "../pages/account/account";
import {User} from "../mapper/user";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  isAuthenticated: boolean = false;
  user: User;
  rootPage: any = AccountPage;

  pages: Array<{title: string, icon: string, component: any}> = [];

  constructor(public platform: Platform, private auth: AuthProvider) {
    this.initializeApp();
    APP_MENUS.sidebar.forEach(el => {
      this.pages.push({title: el.label, icon: el.icon, component: el.component});
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      this.auth.onSessionStore.subscribe(() => {
        this.isAuthenticated = this.auth.isAuthenticated;
        if (this.isAuthenticated == true) {
          this.user = new User(this.auth.currentUser);
        }
      });

      this.auth.ready().then(() => {
        this.isAuthenticated = this.auth.isAuthenticated;
        if (this.isAuthenticated == true) {
          this.user = new User(this.auth.currentUser);
        } else {
          this.nav.setRoot(LoginPage);
        }
      });
    });
  }

  logout() {
    this.nav.setRoot(LoginPage).then(() => {
      this.auth.destroyUserCredentials()
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
