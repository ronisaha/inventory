import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler, Platform} from 'ionic-angular';
import {MyApp} from './app.component';
import {LoginPage} from "../pages/login/login";
import {ProductsPage} from "../pages/products/products";
import {SalesPage} from "../pages/sales/sales";
import {RegistrationPage} from "../pages/registration/registration";
import {AuthProvider} from "../providers/auth-provider";
import {DbProvider} from "../providers/db-provider";
import {ProductProvider} from "../providers/product-provider";
import {UserProvider} from "../providers/user-provider";
import { Storage } from '@ionic/storage';
import {AccountPage} from "../pages/account/account";
import {ModalProductCreatePage} from "../pages/products/create/product-create";
import {ModalProductEditPage} from "../pages/products/edit/product-edit";
import {ModalAccountEditPage} from "../pages/account/edit/account-edit";
import {ImageInputComponent} from "../components/image-input/image-input";

export function provideStorage() {
    return new Storage(['websql', 'indexeddb']);
}

export function createDbProvider(platform: Platform) {
    return new DbProvider(platform);
}
export function createAuthProvider(platform: Platform, storage: Storage, userProvider:UserProvider) {
    return new AuthProvider(platform, storage, userProvider);
}
export function createUserProvider(db: DbProvider) {
    return new UserProvider(db);
}

@NgModule({
    declarations: [
        MyApp, LoginPage, ProductsPage, SalesPage, RegistrationPage, AccountPage,
        ModalProductCreatePage, ModalProductEditPage, ModalAccountEditPage, ImageInputComponent
    ],
    imports: [
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp, LoginPage, ProductsPage, SalesPage, RegistrationPage, AccountPage,
        ModalProductCreatePage, ModalProductEditPage, ModalAccountEditPage, ImageInputComponent
    ],
    providers: [
        { provide: Storage, useFactory: provideStorage },
        {
            provide: DbProvider,
            useFactory:(createDbProvider),
            deps: [Platform]
        },
        {
            provide: UserProvider,
            useFactory:(createUserProvider),
            deps: [DbProvider]
        },
        {
            provide: AuthProvider,
            useFactory:(createAuthProvider),
            deps: [Platform, Storage, UserProvider]
        },
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        ProductProvider
    ]
})
export class AppModule {
}
