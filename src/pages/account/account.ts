import {Component} from '@angular/core';
import {User} from "../../mapper/user";
import {AuthProvider} from "../../providers/auth-provider";
import {ModalController} from "ionic-angular";
import {ModalAccountEditPage} from "./edit/account-edit";

@Component({
    selector: 'page-account',
    templateUrl: 'account.html'
})
export class AccountPage {

    user: User;

    constructor(private auth: AuthProvider, public modalCtrl: ModalController) {
        this.auth.ready().then(() => {
            this.user = this.auth.currentUser;
        });
    }

    edit(user:User) {
        let modal = this.modalCtrl.create(ModalAccountEditPage, {user:user});
        modal.present();
    }
}
