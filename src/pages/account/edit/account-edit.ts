import {Component} from '@angular/core';

import {ViewController, NavParams} from 'ionic-angular';
import {FormGroup, Validators, FormBuilder} from "@angular/forms";
import {User} from "../../../mapper/user";
import {UserProvider} from "../../../providers/user-provider";
import {AuthProvider} from "../../../providers/auth-provider";

@Component({
    template: `
<ion-header>
  <ion-toolbar>
    <ion-title>
      Edit Account Info
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span ion-text color="primary">Cancel</span>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>
<ion-content padding>
 <form [formGroup]="userForm" (ngSubmit)="save()">

        <ion-item no-padding>
            <ion-label floating>Password</ion-label>
            <ion-input type="password" formControlName="password"></ion-input>
        </ion-item>

        <ion-item no-padding>
            <ion-label floating>Full Name</ion-label>
            <ion-input type="text" formControlName="fullName"></ion-input>
        </ion-item>

        <ion-item no-padding>
            <image-input formControlName="photo">Profile Picture</image-input>
        </ion-item>
        
        <ion-item no-padding>
            <ion-label>Gender</ion-label>
            <ion-select formControlName="gender">
                <ion-option value="Male">Male</ion-option>
                <ion-option value="Female">Female</ion-option>
            </ion-select>
        </ion-item>

        <button ion-button type="submit" block>
            <ion-icon name="md-person-add"></ion-icon>
            &nbsp;Save
        </button>
    </form>
</ion-content>
`
})
export class ModalAccountEditPage {

    user: User;
    userForm: FormGroup;

    constructor(public users: UserProvider,
                private formBuilder: FormBuilder,
                params: NavParams,
                private auth: AuthProvider,
                public viewCtrl: ViewController) {
        this.user = params.get("user");
        this.userForm = this.formBuilder.group({
            password: ['', Validators.required],
            fullName: [this.user.fullName, Validators.required],
            photo: [this.user.photo],
            gender: [this.user.gender, Validators.required],
        });
    }

    save() {
        let trimmedPassword = this.userForm.value.password.trim();
        if (trimmedPassword.length > 0) {
            this.user.password = trimmedPassword;
        }

        if (this.isNotBlank(this.userForm.value.photo)) {
            this.user.photo = this.userForm.value.photo;
        }

        this.user.fullName = this.userForm.value.fullName;
        this.user.gender = this.userForm.value.gender;
        this.users.create(this.user).then(x => {
            this.auth.registerUserSession(this.user);
            this.dismiss();
        });
    }

    isNotBlank(photo) {
        return photo != "undefined" && photo != "";
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
