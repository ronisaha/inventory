import { Component } from '@angular/core';
import {ProductProvider} from "../../providers/product-provider";

/*
  Generated class for the Sales page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-sales',
  templateUrl: 'sales.html'
})
export class SalesPage {
  items: Array<any> = [];

  constructor(public products: ProductProvider) {}

  ionViewDidLoad() {
    this.products.allSells().then(products => this.items = products);
  }
}
