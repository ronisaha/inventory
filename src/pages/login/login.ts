import {Component} from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import {FormBuilder, Validators, FormGroup} from "@angular/forms";
import {RegistrationPage} from "../registration/registration";
import {AuthProvider} from "../../providers/auth-provider";
import {AccountPage} from "../account/account";

/*
 Generated class for the Login page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {

    loginForm: FormGroup;

    constructor(public navCtrl: NavController,
                private formBuilder: FormBuilder,
                public toastCtrl: ToastController,
                private auth: AuthProvider) {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });
    }

    private presentToast(msg: string) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    login() {
        this.auth.login(this.loginForm.value.username, this.loginForm.value.password).then(
            t => this.navCtrl.setRoot(AccountPage),
            reason => this.presentToast("Incorrect username or password!")
        )
    }

    signUp() {
        this.navCtrl.push(RegistrationPage);
    }
}
