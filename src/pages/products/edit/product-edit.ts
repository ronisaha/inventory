import {Component} from '@angular/core';

import {ViewController, NavParams} from 'ionic-angular';
import {FormGroup, Validators, FormBuilder} from "@angular/forms";
import {ProductProvider} from "../../../providers/product-provider";
import {Product} from "../../../mapper/product";

@Component({
    template: `
<ion-header>
  <ion-toolbar>
    <ion-title>
      Edit Product
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span ion-text color="primary">Cancel</span>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>
<ion-content padding>
  <form [formGroup]="productForm" (ngSubmit)="save()">
        <ion-item no-padding>
            <ion-label floating>Name</ion-label>
            <ion-input type="text" formControlName="name"></ion-input>
        </ion-item>
        
        <ion-item no-padding>
            <ion-label floating>Description</ion-label>
            <ion-input type="textarea" formControlName="description"></ion-input>
        </ion-item>

        <ion-item no-padding>
            <ion-label floating>Price</ion-label>
            <ion-input type="text" formControlName="price"></ion-input>
        </ion-item>

        <button ion-button type="submit" block>
            <ion-icon name="md-add-circle"></ion-icon>
            &nbsp;Save
        </button>
    </form>
</ion-content>
`
})
export class ModalProductEditPage {

    product: Product;
    productForm: FormGroup;

    constructor(public products: ProductProvider,
                private formBuilder: FormBuilder,
                params: NavParams,
                public viewCtrl: ViewController) {
        this.product = params.get("product");
        console.log(this.product);
        this.productForm = this.formBuilder.group({
            name: [this.product.name, Validators.required],
            description: [this.product.description, Validators.required],
            price: [this.product.price, Validators.required]
        });
    }

    save() {
        this.product.price = this.productForm.value.price;
        this.product.name = this.productForm.value.name;
        this.product.description = this.productForm.value.description;
        this.products.create(this.product).then(x => {
            this.dismiss();
        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}