import { Component } from '@angular/core';

import { ViewController } from 'ionic-angular';
import {FormGroup, Validators, FormBuilder} from "@angular/forms";
import {ProductProvider} from "../../../providers/product-provider";
import {Product} from "../../../mapper/product";

@Component({
    template: `
<ion-header>
  <ion-toolbar>
    <ion-title>
      New Product
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span ion-text color="primary">Cancel</span>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>
<ion-content padding>
  <form [formGroup]="productForm" (ngSubmit)="create()" >
        <ion-item no-padding>
            <ion-label floating>Name</ion-label>
            <ion-input type="text" formControlName="name"></ion-input>
        </ion-item>
        
        <ion-item no-padding>
            <ion-label floating>Description</ion-label>
            <ion-input type="textarea" formControlName="description"></ion-input>
        </ion-item>  
              
        <ion-item no-padding>
            <image-input formControlName="photo"> Capture Image</image-input>
        </ion-item>

        <ion-item no-padding>
            <ion-label floating>Price</ion-label>
            <ion-input type="text" formControlName="price"></ion-input>
        </ion-item>

        <ion-item no-padding>
            <ion-label floating>Stock</ion-label>
            <ion-input type="text" formControlName="stock"></ion-input>
        </ion-item>

        <button ion-button type="submit" block>
            <ion-icon name="md-add-circle"></ion-icon>
            &nbsp;Create
        </button>
    </form>
</ion-content>
`
})
export class ModalProductCreatePage {

    productForm:FormGroup;

    constructor(public products: ProductProvider,
                private formBuilder: FormBuilder,
                public viewCtrl: ViewController) {
        this.productForm = this.formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.required],
            price: ['', Validators.required],
            photo: [''],
            stock: ['', Validators.required]
        });
    }

    create(){
        this.products.create(new Product(this.productForm.value)).then(x => {
            this.dismiss();
        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
