import {Component} from '@angular/core';
import {ModalController, AlertController, ToastController} from 'ionic-angular';
import {ModalProductCreatePage} from "./create/product-create";
import {Product} from "../../mapper/product";
import {ProductProvider} from "../../providers/product-provider";
import {ModalProductEditPage} from "./edit/product-edit";

@Component({
    selector: 'page-products',
    templateUrl: 'products.html'
})
export class ProductsPage {

    productList: Array<Product> = [];

    constructor(public products: ProductProvider,
                public alertCtrl: AlertController,
                public toastCtrl: ToastController,
                public modalCtrl: ModalController) {
    }

    ionViewDidLoad() {
        this.refreshProduct();
    }

  private presentToast(msg:string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  getImageUrl(url) {
    return url && url != "undefined" && url != "" ? url : "assets/icon/logo.png"
  }

  isDefault(url) {
    return !(url && url != "undefined" && url != "")
  }
    private refreshProduct() {
        this.products.all().then(products => this.productList = products);
    }

    createProduct() {
        let modal = this.modalCtrl.create(ModalProductCreatePage);
        modal.present();
        modal.onDidDismiss(x => this.refreshProduct())
    }

  edit(product:Product) {
    let modal = this.modalCtrl.create(ModalProductEditPage, {product:product});
    modal.present();
    modal.onDidDismiss(x => this.refreshProduct())
  }

  sellPrompt(product:Product) {
    let prompt = this.alertCtrl.create({
      title: 'Sell product',
      message: "Enter quantity you want to sell product: " + product.name,
      cssClass: "sell-product",
      inputs: [
        {
          name: 'quantity',
          placeholder: 'Sell Quantity'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sell',
          handler: data => {
            if(data.quantity > product.stock) {
              this.presentToast("You can't sell more then your stock");
              return false;
            }
            this.sellProduct(product, data.quantity);
          }
        }
      ]
    });
    prompt.present();
    prompt.onDidDismiss(x => this.refreshProduct())
  }

  addPrompt(product:Product) {
    let prompt = this.alertCtrl.create({
      title: 'Sell product',
      message: "Enter quantity you want to add for product: " + product.name,
      inputs: [
        {
          name: 'quantity',
          placeholder: 'Quantity'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          handler: data => {
            this.addStock(product.id, data.quantity);
          }
        }
      ]
    });
    prompt.present();
    prompt.onDidDismiss(x => this.refreshProduct())
  }

  sellProduct(product:Product, quantity:number) {
    this.products.sellProduct(product.id, product.price, quantity).then(x => this.refreshProduct());
  }

  addStock(id, quantity:number) {
    this.products.addStock(id, quantity).then(x => this.refreshProduct());
  }
}
