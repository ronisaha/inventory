import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {UserProvider} from "../../providers/user-provider";
import {LoginPage} from "../login/login";

/*
  Generated class for the Registration page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html'
})
export class RegistrationPage {
  signUpForm:FormGroup;

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, private userProvider:UserProvider) {
    this.signUpForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      fullName: ['', Validators.required],
      photo: [''],
      gender: ['', Validators.required],
    });
  }

  signUp() {
    this.userProvider.create(this.signUpForm.value).then((t)=> {
      this.navCtrl.push(LoginPage);
    }, function (reason) {
      console.log(reason);
    })
  }
}
